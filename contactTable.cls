public with sharing class ContactHelper {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList() {
        return [SELECT  Name, 
                Phone
            FROM Contact];
    }
}
